Package              Version
-------------------- -------------------
absl-py              0.11.0
astor                0.8.1
certifi              2020.12.5
cycler               0.10.0
gast                 0.2.2
google-pasta         0.2.0
grpcio               1.31.0
h5py                 2.10.0
importlib-metadata   2.0.0
imutils              0.5.3
Keras                2.3.1
Keras-Applications   1.0.8
Keras-Preprocessing  1.1.0
kiwisolver           1.3.0
Markdown             3.3.3
matplotlib           3.2.1
mkl-fft              1.2.0
mkl-random           1.1.1
mkl-service          2.3.0
numpy                1.19.2
olefile              0.46
opencv-python        4.2.0.34
opt-einsum           3.1.0
Pillow               7.1.2
pip                  20.3.3
protobuf             3.13.0
pyparsing            2.4.7
pyreadline           2.1
python-dateutil      2.8.1
PyYAML               5.3.1
scipy                1.5.2
setuptools           51.1.2.post20210110
six                  1.15.0
tensorboard          2.0.0
tensorflow           2.0.0
tensorflow-estimator 2.0.0
termcolor            1.1.0
tornado              6.1
Werkzeug             0.16.1
wheel                0.36.2
wincertstore         0.2
wrapt                1.12.1
zipp                 3.4.0

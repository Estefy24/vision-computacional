#Editado por: Joana Estefanía Nicolalde P
#correo: jenicolaldep@uce.edu.ec
#fecha:14/12/2020
#obtenido de: https://docs.opencv.org/3.4/d9/db0/tutorial_hough_lines.html
#Transformada de Hough
import sys
import math
import cv2 as cv
import numpy as np
def main(argv):

    #default_file = 'sudoku.png'
    #default_file = 'convergente.jpg'
    default_file = 'tren.jpg'
    filename = argv[0] if len(argv) > 0 else default_file
    # Loads an image
    src = cv.imread(cv.samples.findFile(filename), cv.IMREAD_GRAYSCALE)
    # Check if image is loaded fine
    if src is None:
        print ('Error opening image!')
        print ('Usage: hough_lines.py [image_name -- default ' + default_file + '] \n')
        return -1

    #detecta los bordes
    dst = cv.Canny(src, 50, 200, None, 3)

    # Copy edges to the images that will display the results in BGR
    cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
    cdstP = np.copy(cdst)

    #Transformada de Hough Standar
    #lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)
    lines = cv.HoughLines(dst, 1, np.pi / 180, 200, None, 0, 0)
    #PARÁMETROS
    #dst: Salidadel detectordebordes.Debeseruna Imagen en escala de grises(aunque en realidad es binaria)
    #lineas : Un vector que almacenará los parámetros( r , θ ) de las líneas detectadas
    #rho : la resolución del parámetro r en píxeles. Usamos 1 píxel.
    #theta : la resolución del parámetroθen radianes. Usamos 1 grado (CV_PI / 180)
    #umbral : el número mínimo de intersecciones para "* detectar *" una línea
    #srn y stn : parámetros predeterminados a cero.

    #Dibuja las líneas
    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
            cv.line(cdst, pt1, pt2, (0,0,255), 3, cv.LINE_AA)

    #Transformada de Hough Probabilística
    linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)
    #Parámetros
    #dst : Salida del detector de bordes. Debe ser una imagen en escala de grises (aunque en realidad es binaria)
    #lineas : Un vector que almacenará los parámetros(Xs t a r t,ys t a r t,Xe n d,ye n d) de las líneas detectadas
    #rho : la resolución del parámetroren píxeles. Usamos 1 píxel.
    #theta : la resolución del parámetroθen radianes. Usamos 1 grado (CV_PI / 180)
    #umbral : el número mínimo de intersecciones para "* detectar *" una línea
    #minLinLength : el número mínimo de puntos que pueden formar una línea. Las líneas con menos de este número de puntos no se tienen en cuenta.
    #maxLineGap : el espacio máximo entre dos puntos a considerar en la misma línea
    #Dibujamos las líneas

    if linesP is not None:
        for i in range(0, len(linesP)):
            l = linesP[i][0]
            cv.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0,0,255), 3, cv.LINE_AA)

    cv.imshow("Source", src)
    cv.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)
    cv.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)

    cv.waitKey()
    return 0

if __name__ == "__main__":
    main(sys.argv[1:])
#Editado por: Joana Estefanía Nicolalde P
#correo: jenicolaldep@uce.edu.ec
#fecha:14/12/2020
#Obtenido de: https://omes-va.com/detectando-figuras-geometricas-con-opencv-python/
#Encontrar las figuras geométricas en una imágen.
#Descripción: Se modificó un código para encontrar las figuras geoemtricas como circulo, cudrado, rectangulo con las funciones de
#opencv que ya se han estudiado previamente
import cv2
import random
#image = cv2.imread('figurasColores.png')
#image = cv2.imread('miImagen.png')
image = cv2.imread('tangram.png')
#image = cv2.imread('1.jpg')
#image=cv2.resize(image, (900, 500))

#a escala de grises
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#deteccion de bordes
canny = cv2.Canny(gray, 10, 150)
#para mejorar la imagen
#canny = cv2.dilate(canny, None, iterations=1)
#canny = cv2.erode(canny, None, iterations=1)

#_, th = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY)
#_,cnts,_ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)# OpenCV 3
#para encontrar contornos
cnts,_ = cv2.findContours(canny, cv2.RETR_EXTERNAL-10, cv2.CHAIN_APPROX_SIMPLE)# OpenCV 4
#cv2.drawContours(image, cnts, -1, (0,255,0), 2)
cv2.imshow("cany",canny)
#para las diferentes figuras
for c in cnts:
  epsilon = 0.01*cv2.arcLength(c,True) #toma un porcentaje elegido por prueba y error. se puede modificar para encontrar mejores resultados
  #la función cv2.approxPolyDP aproxima curvas o una curva poligonal con una precisión especificada
  #Epsilon: Es el parámetro que especifica la precisión de aproximación.
  # Para obtener este parámetro necesitaremos de la ayuda de la función cv2.
  # arcLength que a su vez calculará el perímetro del contorno o la longitud de curva (dado el caso).
  #Closed: Es true cuando la curva aproximada es cerrada, es decir que tanto el primer como el último
  # vértice están conectados. False, lo contrario

  approx = cv2.approxPolyDP(c,epsilon,True)

  color=(random.randint(30,255),random.randint(30,255),random.randint(30,255))
  #print(len(approx))
  x,y,w,h = cv2.boundingRect(approx)
  if len(approx)==3:
    cv2.putText(image,'Triangulo', (x,y+75),1,1.5,color,2)
  if len(approx)==4:
    aspect_ratio = float(w)/h
    print('aspect_ratio= ', aspect_ratio)
    if aspect_ratio == 1:
      cv2.putText(image,'Cuadrado', (x,y-5),1,1.5,color,2)
    else:
      cv2.putText(image,'Rectangulo', (x,y-5),1,1.5,color,2)
  if len(approx)==5:
    cv2.putText(image,'Pentagono', (x,y-5),1,1.5,color,2)
  if len(approx)==6:
    cv2.putText(image,'Hexagono', (x,y-5),1,1.5,color,2)
  if len(approx)>20:
    cv2.putText(image,'Circulo', (x,y-5),1,1.5,color,2)

  cv2.drawContours(image, [approx], 0, (100,random.randint(30,255),100),2)
  cv2.imshow('image',image)
  cv2.waitKey(0)

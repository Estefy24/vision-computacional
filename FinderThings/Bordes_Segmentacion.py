#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 08/12/2020

#Segmentación de Imágenes con el algoritmo de K-MEANS
#Descripcion: Leer y generar nuevas imagenes a partir de los diferentes canales, guardarlas en directorios


#importar libreria OpenCv
import cv2
#Funciones de Sistema Operativo
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import math


initSequence=1 #inicio secuencia
numSequences=100 #fin Secuencia
cont_frame=0 #contador auxiliar para implementar

#Directorios de escritura
path_Union="../../Datasets/17Flowers/Union_seg_edges"
#Directorio de lectura de imagenes (ORIGINALES)
path_Edges="../../Datasets/17Flowers/Output_Edges"


path_Segmentacion= "../../Datasets/17Flowers/Output_Segemtation_Kmeans"

#Contabilizar número de archivos de la carpeta originales
total_Images=int(len(os.listdir(path_Edges)))
#numSequences=total_Images
contador=0
#Bucle que recorre desde un inicio hasta un find e directorio path_RGB
for ns in range(initSequence,numSequences+1):
  contador=contador+1
  dir_Images=path_Segmentacion +"/KmeansImage_" +str(ns).zfill(4) + ".jpg"

  dir_Images2 = path_Edges + "/EdgesImage_" + str(ns).zfill(4) + ".jpg"

  #print(dir_Images)
  #leer con openCV cada imagen del directorio
  image =cv2.imread(dir_Images)

  #print(dir_Images2)
  # leer con openCV cada imagen del directorio
  image2 = cv2.imread(dir_Images2)

  #mismonumero de canales
 # image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
  #image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)



  #unimos las imágenes

  union_imagen= cv2.add(image,image2)


  cv2.waitKey(0)
  cv2.destroyAllWindows()


  #Escribir en el directorio
  cv2.imwrite(path_Union + "/UnionImage_"+ str(ns).zfill(4)+".jpg",union_imagen)
  # # Leer canales

  # img_red[:, :, 2] = 0
  # plt.title("Imagenes canal rojo")
  # plt.imshow(img)

#tamaño d eimagen
#print(total_Images)
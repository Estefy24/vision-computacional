#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 08/12/2020
#Imágenes con el algoritmo de Canny- Detección de Bordes
#Descripcion: Leer y generar nuevas imagenes a partir de los diferentes canales, guardarlas en directorios


#importar libreria OpenCv
import cv2 as cv
#Funciones de Sistema Operativo
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

initSequence=1 #inicio secuencia
numSequences=100 #fin Secuencia
cont_frame=0 #contador auxiliar para implementar

#Directorio de lectura de imagenes (ORIGINALES)

path_Kmean="../../Datasets/17Flowers/Output_Segemtation_Kmeans"
#Directorios de escritura

path_bordes_kmean= "../../Datasets/17Flowers/Bordes_Kmeans"

#Contabilizar número de archivos de la carpeta originales
total_Images=int(len(os.listdir(path_Kmean)))
#numSequences=total_Images
contador=0
#Bucle que recorre desde un inicio hasta un find e directorio path_RGB
for ns in range(initSequence,numSequences+1):
  contador=contador+1
  dir_Images=path_Kmean +"/KmeansImage_" +str(ns).zfill(4) + ".jpg"
  print(dir_Images)
  #leer con openCV cada imagen del directorio
  img =cv.imread(dir_Images)
  #RGB=cv.cvtColor(img, cv.COLOR_RGB2HLS)



  #Transforamr una imagen RGB (COLOR) a escala de grises
  bordes = cv.Canny(img, 0, 255)

  cv.waitKey(0)
  cv.destroyAllWindows()


  #Escribir en el directorio
  cv.imwrite(path_bordes_kmean + "/EdgesKmeanImage_"+ str(ns).zfill(4)+".jpg",bordes)
  # # Leer canales

  # img_red[:, :, 2] = 0
  # plt.title("Imagenes canal rojo")
  # plt.imshow(img)

#tamaño d eimagen
#print(total_Images)
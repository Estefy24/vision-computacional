#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 08/12/2020
#Canales de una imagen
#Descripcion: Leer y generar nuevas imagenes a partir de los diferentes canales, guardarlas en directorios

#GRAY-RGB-R-G-B-
#importar libreria OpenCv
import cv2 as cv
#Funciones de Sistema Operativo
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

initSequence=1 #inicio secuencia
numSequences=100 #fin Secuencia
cont_frame=0 #contador auxiliar para implementar

#Directorio de lectura de imagenes (ORIGINALES)

path_RGB="../../Datasets/17Flowers/RGB"
#Directorios de escritura

path_R= "../../Datasets/17Flowers/R"
path_G= "../../Datasets/17Flowers/G"
path_B= "../../Datasets/17Flowers/B"
path_GRAY= "../../Datasets/17Flowers/GRAY"

#Contabilizar número de archivos de la carpeta originales
total_Images=int(len(os.listdir(path_RGB)))
#numSequences=total_Images
contador=0
#Bucle que recorre desde un inicio hasta un find e directorio path_RGB
for ns in range(initSequence,numSequences+1):
  contador=contador+1
  dir_Images=path_RGB +"/image_" +str(ns).zfill(4) + ".jpg"
  print(dir_Images)
  #leer con openCV cada imagen del directorio
  img =cv.imread(dir_Images)
  #RGB=cv.cvtColor(img, cv.COLOR_RGB2HLS)


  # CANAL 1
  img_red = img.copy()  # creo una copia de la imagen para preservar la original
  img_red[:, :, 1] = 0
  img_red[:, :, 0] = 0

  #CANAL 2
  img_green = img.copy()  # creo una copia de la imagen para preservar la original
  img_green[:, :, 0] = 0
  img_green[:, :, 2] = 0

  # CANAL 3
  img_blue = img.copy()  # creo una copia de la imagen para preservar la original
  img_blue[:, :, 1] = 0
  img_blue[:, :, 2] = 0

  # # canal1=  RGB[:, :, 0]
  # # canal2 = RGB[:, :, 1]
  # # canal3 = RGB[:, :, 2]

  #Transforamr una imagen RGB (COLOR) a escala de grises
  gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)
  # cv.imshow("Original",img)
  # cv.imshow("Grises", gray)

 # cv.imshow("R",np.hstack((img_red,img_green,img_blue)))
  #cv.imshow("G",img_green)
  #cv.imshow("B",img_blue)


  #Otra forma de sacar los canales
  #B = cv.extractChannel(img, 0)
  #G = cv.extractChannel(img, 1)
  #R = cv.extractChannel(img, 2)
  #
  #
  #cv.imshow("",np.hstack((R,G,B)))



  cv.waitKey(0)
  cv.destroyAllWindows()

  #Para escribir las nuevas imagenes con su formato adecuado en el directorio seleccionada
  cv.imwrite(path_R + "/RImage_" + str(ns).zfill(4) + ".jpg", img_red)
  cv.imwrite(path_G + "/GImage_" + str(ns).zfill(4) + ".jpg", img_green)
  cv.imwrite(path_B + "/BImage_" + str(ns).zfill(4) + ".jpg", img_blue)

  #Escribir en el directorio
  cv.imwrite(path_GRAY + "/GrayImage_"+ str(ns).zfill(4)+".jpg",gray)
  # # Leer canales

  # img_red[:, :, 2] = 0
  # plt.title("Imagenes canal rojo")
  # plt.imshow(img)

#tamaño d eimagen
#print(total_Images)
#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 08/12/2020

#Segmentación de Imágenes con el algoritmo de WaterSheed
#Descripcion: Leer y generar nuevas imagenes a partir de los diferentes canales, guardarlas en directorios


#importar libreria OpenCv
import cv2
#Funciones de Sistema Operativo
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

initSequence=1 #inicio secuencia
numSequences=100 #fin Secuencia
cont_frame=0 #contador auxiliar para implementar

#Directorio de lectura de imagenes (ORIGINALES)

path_RGB="../../Datasets/17Flowers/RGB"
#Directorios de escritura

path_EDGES= "../../Datasets/17Flowers/Output_Segmentation"

#Contabilizar número de archivos de la carpeta originales
total_Images=int(len(os.listdir(path_RGB)))
#numSequences=total_Images
contador=0
#Bucle que recorre desde un inicio hasta un find e directorio path_RGB
for ns in range(initSequence,numSequences+1):
  contador=contador+1
  dir_Images=path_RGB +"/image_" +str(ns).zfill(4) + ".jpg"
  print(dir_Images)
  #leer con openCV cada imagen del directorio
  img =cv2.imread(dir_Images)
  #RGB=cv.cvtColor(img, cv.COLOR_RGB2HLS)



#ALGORITMO WATERSHED

  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
  # noise removal
  kernel = np.ones((3,3), np.uint8)
  opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)

  # sure background area
  sure_bg = cv2.dilate(opening, kernel, iterations=3)

  # Finding sure foreground area
  dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
  ret, sure_fg = cv2.threshold(dist_transform, 0.7 * dist_transform.max(), 255, 0)

  # Finding unknown region
  sure_fg = np.uint8(sure_fg)
  unknown = cv2.subtract(sure_bg, sure_fg)
  # Marker labelling
  ret, markers = cv2.connectedComponents(sure_fg)

  # Add one to all labels so that sure background is not 0, but 1
  markers = markers + 1

  # Now, mark the region of unknown with zero
  markers[unknown == 255] = 0
  markers = cv2.watershed(img, markers)
  img[markers == -1] = [255, 0, 0]

















  cv2.waitKey(0)
  cv2.destroyAllWindows()


  #Escribir en el directorio
  cv2.imwrite(path_EDGES + "/WaterShedImage_"+ str(ns).zfill(4)+".jpg",img)
  # # Leer canales

  # img_red[:, :, 2] = 0
  # plt.title("Imagenes canal rojo")
  # plt.imshow(img)

#tamaño d eimagen
#print(total_Images)
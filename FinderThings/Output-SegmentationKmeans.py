#autor: Joana Estefania Nicolalde Perugachi
#Fecha: 08/12/2020

#Segmentación de Imágenes con el algoritmo de K-MEANS
#Descripcion: Leer y generar nuevas imagenes a partir de los diferentes canales, guardarlas en directorios


#importar libreria OpenCv
import cv2
#Funciones de Sistema Operativo
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

initSequence=1 #inicio secuencia
numSequences=100 #fin Secuencia
cont_frame=0 #contador auxiliar para implementar

#Directorio de lectura de imagenes (ORIGINALES)

path_RGB="../../Datasets/17Flowers/RGB"
#Directorios de escritura

path_EDGES= "../../Datasets/17Flowers/Output_Segemtation_Kmeans"

#Contabilizar número de archivos de la carpeta originales
total_Images=int(len(os.listdir(path_RGB)))
#numSequences=total_Images
contador=0
#Bucle que recorre desde un inicio hasta un find e directorio path_RGB
for ns in range(initSequence,numSequences+1):
  contador=contador+1
  dir_Images=path_RGB +"/image_" +str(ns).zfill(4) + ".jpg"
  print(dir_Images)
  #leer con openCV cada imagen del directorio
  image =cv2.imread(dir_Images)
  #RGB=cv.cvtColor(img, cv.COLOR_RGB2HLS)



#ALGORITMO KMEANS

  # canales para evitar problema en el plot
  b, g, r = cv2.split(image)
  image = cv2.merge([r, g, b])
  # Creamos una copia para poderla manipular a nuestro antojo.
  image_copy = np.copy(image)

  # Mostramos la imagen y esperamos que el usuario presione cualquier tecla para continuar.
  # cv2.imshow("Imagen", image)
  # cv2.waitKey(0)

  # Convertiremos la imagen en un arreglo de ternas, las cuales representan el valor de cada pixel. En pocas palabras,
  # estamos aplanando la imagen, volviéndola un vector de puntos en un espacio 3D.
  pixel_values = image_copy.reshape((-1, 3))
  pixel_values = np.float32(pixel_values)

  # Abajo estamos aplicando K-Means. Como siempre, OpenCV es un poco complicado en su sintaxis, así que vamos por partes.

  # Definimos el criterio de terminación del algoritmo. En este caso, terminaremos cuando la última actualización de los
  # centroides sea menor a *epsilon* (cv2.TERM_CRITERIA_EPS), donde epsilon es 1.0 (último elemento de la tupla), o bien
  # cuando se hayan completado 10 iteraciones (segundo elemento de la tupla, criterio cv2.TERM_CRITERIA_MAX_ITER).
  stop_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

  # Este es el número de veces que se correrá K-Means con diferentes inicializaciones. La función retornará los mejores
  # resultados.
  number_of_attempts = 10

  # Esta es la estrategia para inicializar los centroides. En este caso, optamos por inicialización aleatoria.
  centroid_initialization_strategy = cv2.KMEANS_RANDOM_CENTERS

  # Ejecutamos K-Means con los siguientes parámetros:
  # - El arreglo de pixeles.
  # - K o el número de clusters a hallar.
  # - None indicando que no pasaremos un arreglo opcional de las mejores etiquetas.
  # - Condición de parada.
  # - Número de ejecuciones.
  # - Estrategia de inicialización.
  #
  # El algoritmo retorna las siguientes salidas:
  # - Un arreglo con la distancia de cada punto a su centroide. Aquí lo ignoramos.
  # - Arreglo de etiquetas.
  # - Arreglo de centroides.
  _, labels, centers = cv2.kmeans(pixel_values,
                                  8,
                                  None,
                                  stop_criteria,
                                  number_of_attempts,
                                  centroid_initialization_strategy)

  # Aplicamos las etiquetas a los centroides para segmentar los pixeles en su grupo correspondiente.
  centers = np.uint8(centers)
  segmented_data = centers[labels.flatten()]

  # Debemos reestructurar el arreglo de datos segmentados con las dimensiones de la imagen original.
  segmented_image = segmented_data.reshape(image_copy.shape)

  cv2.waitKey(0)
  cv2.destroyAllWindows()


  #Escribir en el directorio
  cv2.imwrite(path_EDGES + "/KmeansImage_"+ str(ns).zfill(4)+".jpg",segmented_image)
  # # Leer canales

  # img_red[:, :, 2] = 0
  # plt.title("Imagenes canal rojo")
  # plt.imshow(img)

#tamaño d eimagen
#print(total_Images)
#autor/Editado por: Joana Estefania Nicolalde Perugachi
#Fecha: 08/12/2020
#Deteccion de Objetos en Video, ejemplo con red RETINANET con tensorflow
#Se inserta un video para su detección.

from imageai.Detection import VideoObjectDetection
import os
import tensorflow as tf
execution_path = os.getcwd()

#libera la memoria de la tarjeta
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
    #from tensorflow import ConfigProto
    #from tensorflow import InteractiveSession
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)



#config = tf.ConfigProto()
#config.gpu_options.allow_growth = True
#sess = tf.Session(config=config)



detector = VideoObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath( os.path.join(execution_path , "resnet50_coco_best_v2.0.1.h5"))
detector.loadModel()

video_path = detector.detectObjectsFromVideo(input_file_path=os.path.join(execution_path, "traffic.mp4"),
                                output_file_path=os.path.join(execution_path, "detectandoTrafico3")
                                , frames_per_second=30, log_progress=True)
#video_path = detector.detectObjectsFromVideo(input_file_path=os.path.join(execution_path, "perrosGatos.mp4"),
 #                               output_file_path=os.path.join(execution_path, "traffic_detected")
  #                              , frames_per_second=20, log_progress=True)
print(video_path)

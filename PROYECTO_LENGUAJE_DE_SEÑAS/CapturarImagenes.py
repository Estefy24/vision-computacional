#Creación de Dataset capturando las imágenes por cámara web
#Editado por: Joana Nicolalde, Erick Columba, Luis Rodríguez
#Correos: jenicolaldep@uce.edu.ec/edcolumba@uce.edu.ec/lfrodrigueze@uce.edu.ec
#fecha:24/02/2021

#importamos librerías
import cv2
import numpy as np
import imutils
import os
import cv2 as cv
import time


#Carpeta para guardar las imágenes
Datos = 'imagenes_letraU'
if not os.path.exists(Datos):
	print('Carpeta creada: ', Datos)
	os.makedirs(Datos)

#cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)

#dimensiones del rectángulo del área de región
#x1, y1 = 350, 80
#x2, y2 = 600, 143


x1,y1= 370,50
x2,y2=620,300

#variables para el número de imágenes
count = 751
num_imagenes=1000

#Cámara web
cap = cv.VideoCapture(0)
#variable de tiempo
inicio=time.time()
tomandoFotos=False


#Empezamos a capturar los frames
while True :
	#captura el tiempo
	final = time.time()
	tiempo = round(final - inicio, 0)
	#lee cada fotograma
	ret, frame = cap.read()
	if ret == False:  break
	#modo espejo
	frame = cv.flip(frame, 1)
	#variable para reconocer una letra por teclado
	k = cv2.waitKey(1)
	#creación del rectángulo del area de interés
	#v2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 2)
	cv2.rectangle(frame, (x1,y1),(x2,y2), (255, 0, 0), 2) #con dimension del area de interes del otro archivo
	#Si se presiona la letra s, activa para tomar fotos
	if k == ord('s'):
		tomandoFotos=True;
	#Si se presiona la letra f, pone en pausa tomar fotos
	if k==ord("f"):
		tomandoFotos=False
	#Cada 10 fotogramas envía el frame para guardarse
	if(tiempo%7==0):
		imAux = frame.copy() #creamos una copia del fotograma
		objeto = imAux[y1:y2,x1:x2]
		objeto = imutils.resize(objeto, width=200,height=200) #redimensiona la imagen
		#Permite guardar las imagenes mientras no exceda el limite del numero de imagenes dadas
		if (count <= num_imagenes and tomandoFotos):
			cv2.imwrite(Datos + '/objeto_{}.jpg'.format(count), objeto)
			print('Imagen almacenada: ', 'objeto_{}.jpg'.format(count))
			count = count + 1
	#Presionar ESC para cerrar el programa
	if k == 27:
		break
	#Muestra todos los frame
	cv2.imshow('frame',frame)
	cv2.imshow('objeto',objeto)

cap.release()
cv2.destroyAllWindows()

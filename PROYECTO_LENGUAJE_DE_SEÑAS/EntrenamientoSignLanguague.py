#Entrenamiento SIGN LANGUAGUE
#Editado por: Joana Nicolalde, Erick Columba, Luis Rodríguez
#Correos: jenicolaldep@uce.edu.ec/edcolumba@uce.edu.ec/lfrodrigueze@uce.edu.ec
#Obtenido de: https://www.kaggle.com/learn/deep-learning
#fecha:20/02/2021


#Importación de librerías
import keras
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D, Lambda, MaxPool2D, BatchNormalization
from keras.utils import np_utils
from keras.utils.np_utils import to_categorical
from keras.preprocessing.image import ImageDataGenerator
from keras import models, layers, optimizers
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.utils import class_weight
from keras.optimizers import SGD, RMSprop, Adam, Adagrad, Adadelta, RMSprop
from keras.models import Sequential, model_from_json
from keras.layers import Activation,Dense, Dropout, Flatten, Conv2D, MaxPool2D,MaxPooling2D,AveragePooling2D, BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from keras import backend as K
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.applications.inception_v3 import InceptionV3
import os
from glob import glob
import matplotlib.pyplot as plt
import random
import cv2
import pandas as pd
import numpy as np
import matplotlib.gridspec as gridspec
import seaborn as sns
import zlib
import itertools
import sklearn
import itertools
import scipy
import skimage
from skimage.transform import resize
import csv
from tqdm import tqdm
from sklearn import model_selection
from sklearn.model_selection import train_test_split, learning_curve,KFold,cross_val_score,StratifiedKFold
from sklearn.utils import class_weight
from sklearn.metrics import confusion_matrix
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from tqdm import tqdm
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical
from sklearn.utils import shuffle
import seaborn as sns
from keras.callbacks import Callback, EarlyStopping, ReduceLROnPlateau, ModelCheckpoint


# INICIALIZANDO
imageSize=50 #tamaño de la Imagen
train_dir ="imagenes/" #direcciona a la carpeta que contiene las imágenes para el entrenamiento
test_dir="Test/" #direcciona a la carpeta que contiene las imágenes para el test

#DATASET ALS
#Obtenido de: https://www.kaggle.com/grassknoted/asl-alphabet
#Función para obtener los datos (imágenes)
def get_data(folder):
    """
    Load the data and labels from the given folder.
    """
    X = []
    y = []
    #cargamos las letras a utilizar (vocales) y asignamos un label para cada una
    for folderName in os.listdir(folder):
        if not folderName.startswith('.'):
            if folderName in ['A']:
                label = 0
            elif folderName in ['E']:
                label = 1
            elif folderName in ['I']:
                label = 2
            elif folderName in ['O']:
                label = 3
            elif folderName in ['U']:
                label = 4
            for image_filename in tqdm(os.listdir(folder + folderName)):
                img_file = cv2.imread(folder + folderName + '/' + image_filename)
                if img_file is not None:
                    img_file = skimage.transform.resize(img_file, (imageSize, imageSize, 3))
                    img_arr = np.asarray(img_file)
                    X.append(img_arr)
                    y.append(label)
    X = np.asarray(X)
    y = np.asarray(y)
    return X,y

#Enviamos las variables de entrenamiento de la direccion dada
X_train, y_train = get_data(train_dir)
#X_test, y_test= get_data(test_dir) # Too few images
#

X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.3) #20% de las imagenes del entrenamiento
# Encode labels to hot vectors (ex : 2 -> [0,0,1,0,0,0,0,0,0,0])
# Asignamos el número de clases, en este caso 5 de las vocales
y_trainHot = to_categorical(y_train, num_classes = 5)
y_testHot = to_categorical(y_test, num_classes = 5)

# Shuffle data to permit further subsampling
#
X_train, y_trainHot = shuffle(X_train, y_trainHot, random_state=13)
X_test, y_testHot = shuffle(X_test, y_testHot, random_state=13)
X_train = X_train[:30000]
X_test = X_test[:30000]
y_trainHot = y_trainHot[:30000]
y_testHot = y_testHot[:30000]

#multipleImages = glob('input/archiveASL Alphabet/asl_alphabet_train/asl_alphabet_train/A/**')

# %% [code]
#letras de las vocales con los labels
map_characters = {0: 'A', 1: 'E', 2: 'I', 3: 'O', 4: 'U'}
dict_characters=map_characters
df = pd.DataFrame()
df["labels"]=y_train
lab = df['labels']
dist = lab.value_counts()
sns.countplot(lab)
print(dict_characters) #imprime los caracteres

# *Step 5: Evaluate Classification Models*
# %% [code] {"scrolled":true}
map_characters1 = map_characters
class_weight1 = class_weight.compute_class_weight('balanced', np.unique(y_train), y_train)
#Usamos un modelo preentrenado de VGG16
pretrained_model = VGG16(weights ='imagenet', include_top=False, input_shape=(imageSize, imageSize, 3))
#pretrained_model_2 = InceptionV3(weights = weight_path2, include_top=False, input_shape=(imageSize, imageSize, 3))
optimizer1 = keras.optimizers.Adam()
optimizer2 = keras.optimizers.RMSprop(lr=0.0001)

#Método para entrenar la Red, con los parámetros
def pretrainedNetwork(xtrain,ytrain,xtest,ytest,pretrained_model,classweight,numclasses,numepochs,optimizer,labels):
    base_model = pretrained_model # Topless
    # Add top layer
    x = base_model.output
    x = Flatten()(x)
    predictions = Dense(numclasses, activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
    # Train top layer
    for layer in base_model.layers:
        layer.trainable = False
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])
    model.summary()
    batch_size = 16 #defautl 32
    my_callbacks = [
        keras.callbacks.EarlyStopping(patience=2),
        keras.callbacks.ModelCheckpoint(filepath='model.{epoch:02d}-{val_loss:.2f}.h5'),
        keras.callbacks.TensorBoard(log_dir='./logs')
        #keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq = 0,embeddings_freq = 0, update_freq = "epoch")

         # How often to log histogram visualizations
          # How often to log embedding visualizations

    ]

    history = model.fit(xtrain, ytrain, batch_size=batch_size, epochs=numepochs, class_weight=classweight,
                        validation_data=(xtest, ytest),
                        verbose=1, callbacks=my_callbacks)

    #Para guardar el modelo con un nombre en específico
    #model.save('Modelos/miModeloTreinta.h5')

    #Evaluate model
    score = model.evaluate(xtest,ytest, verbose=0)
    print('\nKeras CNN - accuracy:', score[1], '\n')
    y_pred = model.predict(xtest)
    print('\n', sklearn.metrics.classification_report(np.where(ytest > 0)[1], np.argmax(y_pred, axis=1), target_names=list(labels.values())), sep='')
    Y_pred_classes = np.argmax(y_pred,axis = 1)
    Y_true = np.argmax(ytest,axis = 1)

    #devuelve el modelo
    return model

#Llamamos al método enviando todos los parámetros
pretrainedNetwork(X_train, y_trainHot, X_test, y_testHot,pretrained_model,class_weight1,5,20,optimizer1,map_characters1)
# ..................#

#PROYECTO DETECCIÓN DE GESTOS EN TIEMPO REAL DE LAS VOCALES
#Este proyecto utiliza tensorflow, keras, para realizar las predicciones, basándose en un peso .h5 previamente entrenado.
##Desarrollado por: Joana Nicolalde, Erick Columba, Luis Rodríguez
#Correos: jenicolaldep@uce.edu.ec/edcolumba@uce.edu.ec/lfrodrigueze@uce.edu.ec

#Creación de Interfaz de Usuario

#prob
import  sys

from PyQt5 import  uic

from PyQt5.QtWidgets import QMainWindow,QApplication, QDialog

from ventanaInstrucciones import *
from ventanaDeteccionSenas2 import  *



class SignLanguagueGUI(QDialog):
    def __init__(self):
        super().__init__()
        #Permite cargar el interfaz de Usuario
        uic.loadUi("interfazSignLnguague.ui", self)
        #PARA LOS BOTONES
        self.botonSalir.clicked.connect(self.salir)
        self.botonInstrucciones.clicked.connect(self.ventanaInstr)
        self.botonIniciar.clicked.connect(self.ventanaIniciar)


    def salir(self):
        print('Para Salir .')

        self.closeAllWindows()

    def ventanaInstr(self):
        GUI2=ventanaInstrucciones()
        GUI2.exec_()

    def ventanaIniciar(self):
        deteccion=ventanaDeteccionSenas2()
        deteccion.exec_()

if __name__ == "__main__":
    Main=QApplication(sys.argv)
    GUI=SignLanguagueGUI()
    GUI.show()
    sys.exit(Main.exec_())
   
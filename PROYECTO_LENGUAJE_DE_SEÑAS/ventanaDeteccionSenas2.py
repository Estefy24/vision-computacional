#PROYECTO DETECCIÓN DE GESTOS EN TIEMPO REAL DE LAS VOCALES
#Este proyecto utiliza tensorflow, keras, para realizar las predicciones, basándose en un peso .h5 previamente entrenado.
##Editado por: Joana Nicolalde, Erick Columba, Luis Rodríguez
#Correos: jenicolaldep@uce.edu.ec/edcolumba@uce.edu.ec/lfrodrigueze@uce.edu.ec
#fecha: 20/02/2021

#IMPORTACION DE LIBRERÍAS
import cv2 as cv
import numpy as np
import imutils# Utilizamos imutils para REDIMENSIONAR LOS FOTOGRAMAS, SE DEBE INSTALAR "pip install imutils"
import random

#tensorflow
import  threading
from keras.models import load_model
from threading import Thread
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.applications.inception_v3 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from keras.applications.vgg16 import decode_predictions



import  sys

import imutils
from PyQt5 import uic, QtGui
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMainWindow,QApplication, QDialog, QFileDialog
#from Detection_SignLanguague import *
from PyQt5.QtGui import QImage, QPixmap

from PyQt5 import  QtCore



import cv2 as cv

global cap

# Cargamos el modelo
global model
# model = load_model('Modelos/modelDataset.h5')
model = load_model('Modelos/miModeloTreinta.h5')
global frame
global frameAux
global bg
global key

class ventanaDeteccionSenas2(QDialog):
    def __init__(self):
        super().__init__()
        uic.loadUi("ventanaDeteccionImagen.ui", self)
        self.filename = None
        self.tmp = None
        self.botonRegresar.clicked.connect(self.regresar)
        self.botonSalir.clicked.connect(self.salir)
        self.botonTiempoReal.clicked.connect(self.show_video)
        self.botonInsertarImagen.clicked.connect(self.mostrarImagen)

    def keras_process_image(self, image):

        # Cambia el tamaño de la Imagen
        input_shape = (50, 50)
        image = cv.resize(image, input_shape)
        image = img_to_array(image)
        image = np.expand_dims(image, axis=0)
        image = preprocess_input(image)

        return image

    def keras_predict(self, model, image, frame):
        color_start = (204, 204, 0)
        processed = self.keras_process_image(image)
        pred_probab = model.predict(processed)[0]
        pred_class = list(pred_probab).index(max(pred_probab))
        # print(pred_probab)

        if (pred_class == 0):
            # print("A")
            # cv.putText(frame, 'A', (310, 110), 1, 1, color_start, 1, cv.LINE_4)
            panel = cv.imread('ImagenesInterfaz/A.png')
            o = 60
            p = 280
            img_height, img_width, _ = panel.shape
            frame[o:o + img_height, p:p + img_width] = panel

            # Para la probabilidad
            print('%s (%.2f%%)' % (pred_probab[0], pred_probab[0] * 100))
            # Escribe solo el porcenatje

            porcentaje = '%.2f%%' % (pred_probab[0] * 100)

            cv.putText(frame, porcentaje, (290, 170), 1, 1, color_start, 2, cv.LINE_4)
            #
        elif pred_class == 1:
            # cv.putText(frame, 'E', (310, 110), 1, 4, color_start, 3, cv.LINE_4)
            # print("E")
            panel = cv.imread('ImagenesInterfaz/E.png')
            o = 60
            p = 280
            img_height, img_width, _ = panel.shape
            frame[o:o + img_height, p:p + img_width] = panel

            # Probabilidad
            print('%s (%.2f%%)' % (pred_probab[1], pred_probab[1] * 100))

            porcentaje = '%.2f%%' % (pred_probab[1] * 100)

            cv.putText(frame, porcentaje, (290, 170), 1, 1, color_start, 2, cv.LINE_4)

        elif pred_class == 2:
            # cv.putText(frame, 'I', (310, 110), 1, 4, color_start, 3, cv.LINE_4)
            # print("I")

            panel = cv.imread('ImagenesInterfaz/I.png')
            o = 60
            p = 280
            img_height, img_width, _ = panel.shape
            frame[o:o + img_height, p:p + img_width] = panel
            # Probabilidad
            print('%s (%.2f%%)' % (pred_probab[2], pred_probab[2] * 100))
            porcentaje = '%.2f%%' % (pred_probab[2] * 100)
            cv.putText(frame, porcentaje, (290, 170), 1, 1, color_start, 2, cv.LINE_4)

        elif pred_class == 3:
            # cv.putText(frame, 'O', (310, 110), 1, 4, color_start, 3, cv.LINE_4)
            # print("O")
            panel = cv.imread('ImagenesInterfaz/O.png')
            o = 60
            p = 280
            img_height, img_width, _ = panel.shape
            frame[o:o + img_height, p:p + img_width] = panel
            # Probabilidad
            print('%s (%.2f%%)' % (pred_probab[3], pred_probab[3] * 100))
            porcentaje = '%.2f%%' % (pred_probab[3] * 100)
            cv.putText(frame, porcentaje, (290, 170), 1, 1, color_start, 2, cv.LINE_4)


        elif pred_class == 4:
            # cv.putText(frame, 'U', (310, 110), 1, 4, color_start, 3, cv.LINE_4)
            # print("U")
            panel = cv.imread('ImagenesInterfaz/U.png')
            o = 60
            p = 280
            img_height, img_width, _ = panel.shape
            frame[o:o + img_height, p:p + img_width] = panel
            print('%s (%.2f%%)' % (pred_probab[4], pred_probab[4] * 100))
            porcentaje = '%.2f%%' % (pred_probab[4] * 100)
            cv.putText(frame, porcentaje, (290, 170), 1, 1, color_start, 2, cv.LINE_4)

        return max(pred_probab), pred_class

    def keyPressEvent(self, event):
      key = event.key()
      #  if key == QtCore.Qt.Key_0:
       #     #bg = cv.cvtColor(frameAux, cv.COLOR_BGR2GRAY)
        #    print('Entra',bg)



    def show_video(self):
        self.vista_camara.setVisible(True)
        #para que no muestre la imagen
        self.imagenInsertada.setVisible(False)
        self.resultado.setVisible(False)
        color_start = (204, 204, 0)
        cap = cv.VideoCapture(0) #Para capturar video
        # Creamos una variable  que almacene el fondo y nos ayude a realizar sustraccion
        bg = None


        #**********************Sección para mostrar el video en la ventana y poder redimensionarlo
        h, w = 480, 640
        y_offset, x_offset = 100, 120
        ls = 1
        self.vista_camara.resize(w, h)
       #**************************************************************************************



        while True:
            # Capture -by-
            ret,  frame=cap.read()
            frame = imutils.resize(frame, width=640)
            frame = cv.flip(frame, 1)

            cv.rectangle(frame, (x_offset, y_offset), (x_offset + w, y_offset + h), (0, 0, 255), ls)
            frame_cut = frame[y_offset:y_offset + h, x_offset:x_offset + w]

            width = cap.get(3)  # float
            height = cap.get(4)  # float

            print('width, height:', width, height)

            ROI = frame_cut[50:300,380:600]
            cv.rectangle(frame_cut, (365, 50), (515, 250), color_start, 2)

            self.keras_predict(model, ROI, frame_cut)

            image = self.cvimg_to_qtimg(frame_cut)
            self.vista_camara.setPixmap(QPixmap(image).scaled(self.vista_camara.width(), self.vista_camara.height()))

            #self.keras_predict(model, ROI, frame)
            c = cv.waitKey(30) & 0xff
            if c == 27:
                cap.release()
                break

                # Pido que espere un momento entre fotogramas (20) para poder visualizar de mejor manera EL VIDEO

            # Si presionamos i almacenamos el fondo de la escena al presionar la letra i

    def cvimg_to_qtimg(self, cvimg):
        height, width, depth = cvimg.shape
        cvimg = cv.cvtColor(cvimg, cv.COLOR_BGR2RGB)
        cvimg = QImage(cvimg.data, width, height, width * depth, QImage.Format_RGB888)

        return cvimg

    def mostrarImagen(self):

        self.vista_camara.setVisible(False)
        self.imagenInsertada.setVisible(True)

        self.filename=QFileDialog.getOpenFileName(filter="objeto_(*.*)")[0]
        self.image=cv.imread(self.filename)

        self.tmp = self.image
        self.keras_predictImage(self.image) #para la predicción

        image = imutils.resize(self.image, width=200)
        frame = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        image = QImage(frame.data, frame.shape[1], frame.shape[0], frame.strides[0], QImage.Format_RGB888)

        self.imagenInsertada.setPixmap(QtGui.QPixmap.fromImage(image))

    def mostrarImagenResultados(self,pred_class):

        self.resultado.setVisible(True)
        self.vista_camara.setVisible(False)

        resultA = cv.imread('ImagenesInterfaz/A.png')
        resultE= cv.imread('ImagenesInterfaz/E.png')
        resultI=cv.imread('ImagenesInterfaz/I.png')
        resultO=cv.imread('ImagenesInterfaz/O.png')
        resultU= cv.imread('ImagenesInterfaz/U.png')



        if (pred_class == 0):
            print("A")

            image = self.cvimg_to_qtimg(resultA)
            self.resultado.setPixmap(QtGui.QPixmap.fromImage(image))

        if (pred_class == 1):
            print("E")
            image = self.cvimg_to_qtimg(resultE)
            self.resultado.setPixmap(QtGui.QPixmap.fromImage(image))
        if (pred_class == 2):
            print("I")
            image = self.cvimg_to_qtimg(resultI)
            self.resultado.setPixmap(QtGui.QPixmap.fromImage(image))
        if (pred_class == 3):
            print("O")
            image = self.cvimg_to_qtimg(resultO)
            self.resultado.setPixmap(QtGui.QPixmap.fromImage(image))
        if (pred_class == 4):
            print("U")
            image = self.cvimg_to_qtimg(resultU)
            self.resultado.setPixmap(QtGui.QPixmap.fromImage(image))


    def keras_predictImage(self, image):
        color_start = (204, 204, 0)
        processed = self.keras_process_image(image)
        pred_probab = model.predict(processed)[0]
        pred_class = list(pred_probab).index(max(pred_probab))
        # print(pred_probab)
        self.mostrarImagenResultados(pred_class)

    def regresar(self):

        print('Para Regresar .')

        self.close()
        #main = Main()
        #main.exec_()

    def salir(self):
        print('Para Salir .')


        self.closeAllWindows()
        cv.destroyAllWindows()









if __name__ == "__main__":
    ventana=QApplication(sys.argv)
    GUI3=ventanaDeteccionSenas2()
    GUI3.show()
    sys.exit(ventana.exec_())
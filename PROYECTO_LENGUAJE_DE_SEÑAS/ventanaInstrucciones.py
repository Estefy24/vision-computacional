import  sys

from PyQt5 import  uic

from PyQt5.QtWidgets import QMainWindow,QApplication, QDialog



class ventanaInstrucciones(QDialog):
    def __init__(self):
        super().__init__()
        uic.loadUi("ventana2Intrucciones.ui", self)
        self.botonRegresar.clicked.connect(self.regresar)



    def regresar(self):
        print('Para Salir .')

        self.close()



if __name__ == "__main__":
    Main=QApplication(sys.argv)
    GUI2=ventanaInstrucciones()
    GUI2.show()
    sys.exit(Main.exec_())
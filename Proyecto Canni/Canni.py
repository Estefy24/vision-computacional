#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 30/11/2020
#Algoritmo de Canny, para la detección de bordes
#obtenido de  https://docs.opencv.org/master/da/d22/tutorial_py_canny.html

#Importamos las librerías
import cv2
import numpy as np
from matplotlib import pyplot as plt
#el segundo parámetro representa la bandera para leer una imagen en escala de grises

#leemos la imagen de entrada
#img = cv2.imread('messi.png', 0)
img = cv2.imread('leon.jpg', 0)
#img = cv2.imread('sombraas.jpg',0)
#img = cv2.imread('lineas.png', 0)
#img = cv2.imread('mario.png', 0)
#img = cv2.imread('16bits.jpg', 0)
#img = cv2.imread('lab.png', 0)


#Par< aplicar el detector de bordes Canny es necesario una escala de grises
#img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#llamamos a la función definida Canny y enviamos los parámetros necesarios:
#Imagen de entrada, valores de Umbral: minVal, maxVal como se muestra
bordes = cv2.Canny(img, 0,255)
bordes2 = cv2.Canny(img, 100,255)
bordes3 = cv2.Canny(img, 0,500)
bordes4 = cv2.Canny(img, 100,500)
bordes5 = cv2.Canny(img, 300,800)
#180,260
#100,200

#Permite mostrar en un plot las imágenes
plt.subplot(231), plt.imshow(img, cmap='gray')
plt.title('Imagen original'), plt.xticks([]), plt.yticks([])
plt.subplot(232), plt.imshow(bordes, cmap='gray')
plt.title('Bordes de la Imagen 0,255'), plt.xticks([]), plt.yticks([])
plt.subplot(233), plt.imshow(bordes2, cmap='gray')
plt.title('Bordes de la Imagen 100, 255'), plt.xticks([]), plt.yticks([])
plt.subplot(234), plt.imshow(bordes3, cmap='gray')
plt.title('Bordes de la Imagen 0, 500'), plt.xticks([]), plt.yticks([])
plt.subplot(235), plt.imshow(bordes4, cmap='gray')
plt.title('Bordes de la Imagen 100, 500'), plt.xticks([]), plt.yticks([])
plt.subplot(236), plt.imshow(bordes5, cmap='gray')
plt.title('Bordes de la Imagen 300, 800'), plt.xticks([]), plt.yticks([])
#muestra el plot
plt.show()
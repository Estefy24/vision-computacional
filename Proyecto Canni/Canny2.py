#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 1/12/2020
#Algoritmo de Canny, para la detección de bordes, con barras para los parámetros de umbralización
#obtenido de  https://riptutorial.com/opencv/example/29159/canny-edge-thresholds-prototyping-using-trackbars



""" 
CannyTrackbar function allows for a better understanding of 
the mechanisms behind Canny Edge detection algorithm and rapid
prototyping. The example includes basic use case.

2 of the trackbars allow for tuning of the Canny function and
the other 2 help with understanding how basic filtering affects it.
"""

#importamos librerías
import cv2

#funciones
def empty_function(*args):
    pass

#funcion para el funcionamiento del programa, que tiene de parámetro de entrada una imagen.
def CannyTrackbar(img):
    win_name = "*--Canny--*"

    cv2.namedWindow(win_name) #envia el nombre a la ventana
    cv2.resizeWindow(win_name, 500,100) #el tamaño de la ventana

    #Creación de las diferentes barras
    cv2.createTrackbar("minV", win_name, 0, 255, empty_function)
    cv2.createTrackbar("maxV", win_name, 0, 255, empty_function)
    cv2.createTrackbar("blur_size", win_name, 0, 255, empty_function)
    cv2.createTrackbar("blur_amp", win_name, 0, 255, empty_function)

    while True:
        #colocamos  las barras
        cth1_pos = cv2.getTrackbarPos("minV", win_name)
        cth2_pos = cv2.getTrackbarPos("maxV", win_name)
        bsize_pos = cv2.getTrackbarPos("blur_size", win_name)
        bamp_pos = cv2.getTrackbarPos("blur_amp", win_name)

        #aplicamos un filtro gausiano
        img_blurred = cv2.GaussianBlur(img.copy(), (7, 7), bamp_pos)
        #Canny con variación de los parámetros con las barras
        canny = cv2.Canny(img_blurred, cth1_pos, cth2_pos)
        cv2.imshow(win_name, canny)

        key = cv2.waitKey(1) & 0xFF
        if key == ord("c"):
            break

    cv2.destroyAllWindows()
    return canny

img = cv2.imread("leon.jpg")
canny = CannyTrackbar(img)
cv2.imwrite("result.jpg", canny)
#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 30/11/2020
#Algoritmo de Canny, para la detección de bordes y el conteo de objetos, ene ste caso monedas
#obtenido de  https://programarfacil.com/blog/vision-artificial/detector-de-bordes-canny-opencv/

#Importamos la libreria
import cv2
from matplotlib import pyplot as plt

# Cargamos la imagen
#original = cv2.imread("monedas.jpg")
original = cv2.imread("monedas2.jpg")
#cv2.imshow("original", original)#muestra la imagen original a color

# Convertimos a escala de grises
gris = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)

# Aplicar suavizado Gaussiano
gauss = cv2.GaussianBlur(gris, (5, 5), 0)

#cv2.imshow("suavizado", gauss)#muestra la imagen con suavisado gausaiano


# Detectamos los bordes con Canny
#canny = cv2.Canny(gauss, 50, 150)#envia los parámetros exactos para la deeteccion del borde de la imagen
canny = cv2.Canny(gauss, 100, 280)

#cv2.imshow("canny", canny)#muestra los resultados

# Buscamos los contornos
(contornos, _) = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Mostramos el número de monedas por consola
print("He encontrado {} objetos".format(len(contornos)))




#Permite mostrar en un plot las imágenes
plt.subplot(221), plt.imshow(original, cmap='gray')
plt.title('Imagen original'), plt.xticks([]), plt.yticks([])
plt.subplot(222), plt.imshow(gauss, cmap='gray')
plt.title('Gauss'), plt.xticks([]), plt.yticks([])
plt.subplot(223), plt.imshow(canny, cmap='gray')
plt.title('Canny'), plt.xticks([]), plt.yticks([])


#dibuja los contornos en la imagen original
cv2.drawContours(original, contornos, -1, (0, 0, 255), 2)
#cv2.imshow("contornos", original)


plt.subplot(224), plt.imshow(original, cmap='gray')
plt.title('Dibujo'), plt.xticks([]), plt.yticks([])

#muestra el plot
plt.show()


cv2.waitKey(0)
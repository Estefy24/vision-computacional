#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 30/11/2020
#Algoritmo de Canni en video, tiempo real
#importamos las librerías
import cv2

captura = cv2.VideoCapture(0)

def empty_function(*args):
  pass

def CannyTrackbar(captura):
    win_name = "*--Canny--*"

    cv2.namedWindow(win_name)  # envia el nombre a la ventana
    cv2.resizeWindow(win_name, 500, 100)  # el tamaño de la ventana

  # Creación de las diferentes barras
    cv2.createTrackbar("minV", win_name, 0, 255, empty_function)
    cv2.createTrackbar("maxV", win_name, 0, 255, empty_function)
    #cv2.createTrackbar("blur_size", win_name, 0, 255, empty_function)
    cv2.createTrackbar("blur_amp", win_name, 0, 255, empty_function)

    while (captura.isOpened()):
    # lee los fotogramas de la imagen
      ret, imagen = captura.read()
    # cambiamos a escala de grises
      imagen = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)

    # colocamos  las barras
      cth1_pos = cv2.getTrackbarPos("minV", win_name)
      cth2_pos = cv2.getTrackbarPos("maxV", win_name)
      bsize_pos = cv2.getTrackbarPos("blur_size", win_name)
      bamp_pos = cv2.getTrackbarPos("blur_amp", win_name)

    # aplicamos un filtro gausiano
      img_blurred = cv2.GaussianBlur(imagen, (7, 7), bamp_pos)
    # Canny con variación de los parámetros con las barras
      canny = cv2.Canny(img_blurred, cth1_pos, cth2_pos)

      cv2.imshow(win_name, canny)

      key = cv2.waitKey(1) & 0xFF
      if key == ord("c"):
        break

    # llamamos a la función definida Canny y enviamos los parámetros necesarios:
    # Imagen de entrada, valores de Umbral: minVal, maxVal como se muestra
    # imagen = cv2.GaussianBlur(imagen, (7, 7), 1.41)
    # imagen = cv2.Canny(imagen, 25, 75)


    captura.release()
    cv2.destroyAllWindows()

  # permite activar la cámara



canny = CannyTrackbar(captura)
#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 1/12/2020
#Algoritmo de Canny, para la detección de bordes, con parámetros automáticos
#obtenido de  https://riptutorial.com/opencv/example/29159/canny-edge-thresholds-prototyping-using-trackbars



#importamos los paquetes necesarios
import numpy as np
import cv2
from matplotlib import pyplot as plt
#Esta función requiere un solo argumento,imagen , que es la imagen de un solo canal en la que queremos detectar imágenes.
# Un argumento opcional, sigma  se puede utilizar para variar los umbrales de porcentaje que se determinan en base a estadísticas simples.
#Estos umbrales se construyen con base en los +/- porcentajes controlados por elsigma  argumento.

#Un valor menor de sigma  indica un umbral más estricto, mientras que un valor mayor de sigma  da un umbral más amplio.
# En general, no tendrá que cambiar estesigma  valor a menudo. Simplemente seleccione una sola, predeterminadasigma
# valor y aplicarlo a todas las imágenes

def auto_canny(image, sigma=0.33):

    #en la práctica,sigma = 0,33  tiende a dar buenos resultados en la mayor parte del conjunto de datos con
    # el que estoy trabajando, por lo que elijo proporcionar el 33% como valor sigma predeterminado.


	# calcular la mediana de las intensidades de píxeles de un solo canal
	v = np.median(image)
	# aplicar la detección automática de bordes Canny utilizando la mediana calculada
    # tomamos este valor mediano y construimos dos umbrales, inferior  y Superior
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))

	edged = cv2.Canny(image, lower, upper) #Ahora que tenemos nuestros umbrales inferior y superior,
    # aplicamos el detector de bordes Canny

	return edged

#Leemos la imagen de entrada
#image = cv2.imread("lineas.png")
#image = cv2.imread("messi.png")
#image = cv2.imread("sombraas.jpg")
image = cv2.imread("monedas_}.jpg")
image = cv2.imread("monedas2.jpg")

#convertimos la imagen a escala de grises
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#aplicamos un filtro gausiano
blurred = cv2.GaussianBlur(gray, (3, 3), 0)


# aplique la detección de bordes de Canny usando un umbral amplio, ajustado
# umbral y umbral determinado automáticamente
wide = cv2.Canny(blurred, 10, 200) #umbral amplio
tight = cv2.Canny(blurred, 225, 250)#umbral estrecho

auto = auto_canny(blurred)#umbral automatico

# Buscamos los contornos
(contornos, _) = cv2.findContours(auto.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# Mostramos el número de monedas por consola
print("He encontrado {} objetos".format(len(contornos)))


	# show the images
#cv2.imshow("Original", image)
#cv2.imshow("Edges", np.hstack([wide, tight, auto]))

#Permite mostrar en un plot las imágenes
plt.subplot(221), plt.imshow(image, cmap='gray')
plt.title('Imagen original'), plt.xticks([]), plt.yticks([])
plt.subplot(222), plt.imshow(wide, cmap='gray')
plt.title('Umbral Amplio'), plt.xticks([]), plt.yticks([])
plt.subplot(223), plt.imshow(tight, cmap='gray')
plt.title('Umbral Estrecho'), plt.xticks([]), plt.yticks([])
plt.subplot(224), plt.imshow(auto, cmap='gray')
plt.title('Umbral Automatico'), plt.xticks([]), plt.yticks([])

#muestra el plot
plt.show()


cv2.waitKey(0)



# Editado por: Joana Estefanía Nicolalde Perugachi
# Correo: jenicolaldep@uce.edu.ec
# Fecha: 02/12/2020
# Red Imagenet, vgg19,resnet50
# Encontrar Objetos

from keras.applications.vgg16 import VGG16
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg19 import VGG19
from keras.preprocessing import image
from keras.applications.vgg19 import preprocess_input
from keras.models import Model
from keras.applications.vgg16 import preprocess_input, decode_predictions
from keras.applications.resnet50 import preprocess_input, decode_predictions
from context import context 
import numpy as np
import os
from pandas import DataFrame

def find_objects(path, net):
#model = VGG16(weights='imagenet', include_top=False)
    for root, dirs, files in os.walk(path, topdown=False):
        Vcontext = list()
        cont=0
        print("Begin directory")
        for name in files:
            if cont == 0 :
                       Vcontext.append(context("Id"," Label",0))
                       cont = cont+1    
            img_path = os.path.join(root, name) 
            print(img_path)
            
            if net ==0:
                print ("Xception Model ")

            if net ==1:
                print ("VGG16 Model")
                model = VGG16(weights='imagenet')         

            if net ==3:
                print ("VGG19 Model")
                base_model = VGG19(weights='imagenet')
                model = Model(inputs=base_model.input, outputs=base_model.get_layer('block4_pool').output)
            if net ==4:
                print ("ResNet50 Model")
                model = ResNet50(weights='imagenet')

            if net ==5:
                print ("InceptionV3 Model")

            if net ==6:
                print ("InceptionResNetV2 Model")

            if net ==7:
                print ("MobileNet Model")

            if net ==8:
                print ("DenseNet Model")

            if net ==9:
                print ("NASNet Model")

            if net ==10:
                print ("MobileNetV2 Model")

                
            #model = VGG16(weights='imagenet')         
            img = image.load_img(img_path, target_size=(224, 224))
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            x = preprocess_input(x)
            preds = model.predict(x)
            print('--SCENE CATEGORIES Objects: (id, description, prob)')
            lista = decode_predictions(preds, top=5)[0]
            # output the prediction
            for id_label,desc_label,probs in lista:
                print(id_label,desc_label,probs)
                #Vcontext.append(context(id_label,desc_label,probs))
                Vcontext = lista
            #Vcontext.sort(key=lambda context: context.label)
            print ('File completed')
        print (Vcontext)
        #for j in Vcontext:
         #   print (j)                
        data_transposed = zip(*Vcontext)
        df = DataFrame(Vcontext, columns=['id','Desc','Mean_Prob'])
        #pivot = df1.pivot_table(index=['Desc'], values=['Prob'], aggfunc='sum')
        pivot = df.pivot_table(index=['Desc'], values=['Mean_Prob'], aggfunc='mean')
        print(pivot)
    Vcontext.clear()
    
    
    
#def find_objects_ResNet50(path):
#    for root, dirs, files in os.walk(path, topdown=False):
#        Vcontexts = list()
#        cont=0
#        print("Inicio de directorio")
#        for name in files:
#            if cont == 0 :
#                       Vcontexts.append(Contexto("Id"," Label",0))
#                       cont = cont+1    
#            img_path = os.path.join(root, name) 
#            print(img_path)
#            
#            model = ResNet50(weights='imagenet')        
#            img = image.load_img(img_path, target_size=(224, 224))
#            x = image.img_to_array(img)
#            x = np.expand_dims(x, axis=0)
#            x = preprocess_input(x)
#            preds = model.predict(x)
#            print('--SCENE CATEGORIES: (id, description, prob)')
#            lista = decode_predictions(preds, top=5)[0]
#            # output the prediction
#            for id_label,desc_label,probs in lista:
#                print(id_label,desc_label,probs)
#                Vcontexts.append(Contexto(id_label,desc_label,probs))
#            Vcontexts.sort(key=lambda Contexto: Contexto.etiqueta)
#            print ('File completed')
#        for j in Vcontexts:
#            print (j)                
#    Vcontexts.clear()
#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 02/12/2020
#Red AlexNet

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 19:30:25 2019

@author: felipe
"""

# llamada a la red AlexNet

from nets.alexNet import AlexNet
from keras.utils import plot_model

model_of = AlexNet()
model_of.summary()
plot_model(model_of, to_file='./graph_models/AlexNet_model_plot.png', show_shapes=True, show_layer_names=True)
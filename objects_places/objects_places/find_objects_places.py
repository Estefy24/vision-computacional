#Editado por: Joana Estefanía Nicolalde Perugachi
#Correo: jenicolaldep@uce.edu.ec
#Fecha: 02/12/2020
#Encontrar Lugares en video
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 20:45:49 2018

@author: felipe
"""
# Models for image classification with weights trained on ImageNet:
# 0 Xception unavailable
# 1 VGG16 available
# 2 VGG19 available
# 3 ResNet50 available
# 4 InceptionV3 unavailable
# 5 InceptionResNetV2 unavailable
# 6 MobileNet unavailable
# 7 DenseNet unavailable
# 8 NASNet unavailable
# 9 MobileNetV2 unavailable
# find_objects(path, net) 
#from Imagenet_Models import find_objepip install tensorflowcts
#from  places365_VGG16 import find_places
from  places_objects import find_places
# from Imagenet_Models import find_objects_ResNet50
#find_objects("./videos1",1)

find_places("./videos1")

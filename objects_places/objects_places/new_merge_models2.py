# Editado por: Joana Estefanía Nicolalde Perugachi
# Correo: jenicolaldep@uce.edu.ec
# Fecha: 02/12/2020
# Places365 con red vgg16- gráfico plot

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 14 09:56:05 2019

@author: felipe
"""
#from vgg16 import VGG16
from keras.applications.vgg16 import VGG16
#from vgg16_places_365 import VGG16_Places365
from VGG16_Places365 import VGG16_Places365
from keras.models import Model
from keras.layers import Concatenate, Dense
from keras.utils import plot_model

# Entradas de las redes im y pl
model_pl = VGG16_Places365(weights='places')
model_pl.summary()
plot_model(model_pl, to_file='./graph_models/model_pl_model_plot.png', show_shapes=True, show_layer_names=True)

model_im = VGG16(include_top=True, weights='imagenet')
model_im.summary()
plot_model(model_im, to_file='./graph_models/model_im_plot.png', show_shapes=True, show_layer_names=True)

# Cambiar nombre a capas del modelo model_im
for layer in model_im.layers:
    layer.name = layer.name + str("_2")
        
#left_branch = model_pl.layers[-6].output
#right_branch = model_im.layers[-4].output
left_branch = model_pl.layers[-1].output
right_branch = model_im.layers[-1].output    
concatenate_layer = Concatenate()([left_branch, right_branch])

output = Dense(1361)(concatenate_layer)

model = Model(inputs=[model_pl.input, model_im.input], outputs=output)
model.summary()
plot_model(model, to_file='./graph_models/MERGE_model_plot.png', show_shapes=True, show_layer_names=True)

# Editado por: Joana Estefanía Nicolalde Perugachi
# Correo: jenicolaldep@uce.edu.ec
# Fecha: 02/12/2020
# Places365 con red vgg16- gráfico plot
#Encontrar lugares
import numpy as np
import os
from VGG16_Places365 import VGG16_Places365
from keras.preprocessing import image
from places_utils import preprocess_input
from context import context 

def find_places(path):
    for root, dirs, files in os.walk(path, topdown=False):
        Vcontext = list()
        cont=0
        print("Begin directory")
        for name in files:
            if cont == 0 :
                       Vcontext.append(context("Id"," Label",0))
                       cont = cont+1    
            img_path = os.path.join(root, name) 
            print(img_path)
            img = image.load_img(img_path, target_size=(224, 224)) 
            x =image.img_to_array(img) 
            x = np.expand_dims(x, axis=0) 
            x = preprocess_input(x) 
            #print (x.ndim, x.shape, 'x')
            model = VGG16_Places365(weights='places')
            predictions_to_return = 5
            preds = model.predict(x)[0]
            probs = np.sort(preds)[::-1][0:5]
            #print ('preds', preds.ndim, preds.shape, np.sort(preds)[::-1][0:5])
            top_preds = np.argsort(preds)[::-1][0:predictions_to_return]
           # print ('top_preds', top_preds.ndim, top_preds.shape, top_preds) 
            	# load class label 
            file_name ='categories_places365.txt'
            if not os.access(file_name, os.W_OK):
                synset_url = 'Caution-https://raw.githubusercontent.com/CSAILVision/places365/master/categories_places365.txt'
                os.system('wget ' + synset_url)            
            classes = list()
            with open(file_name) as class_file:
                for line in class_file:
                    classes.append(line.strip().split(' ')[0][3:]) 
                classes = tuple(classes) 
            	#print classes 
                print('--SCENE CATEGORIES Places: (id, description, prob)') 
            	# output the prediction
                for i in range(0, 5):    
                   print(top_preds[i], classes[top_preds[i]],probs[i])
                   Vcontext.append(context(top_preds[i],classes[top_preds[i]],probs[i]))
                Vcontext.sort(key=lambda context: context.label)
                print ('File completed')
        for j in Vcontext:
            print (j)                
    Vcontext.clear()
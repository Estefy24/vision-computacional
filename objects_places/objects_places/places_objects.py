# Editado por: Joana Estefanía Nicolalde Perugachi
# Correo: jenicolaldep@uce.edu.ec
# Fecha: 02/12/2020
# Places365 con red vgg16- gráfico plot

import numpy as np
import os
from keras.layers import add, Dense
#from keras.engine import merge

from keras.models import Sequential
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input, decode_predictions
from VGG16_Places365 import VGG16_Places365
from keras.preprocessing import image
from places_utils import preprocess_input
from context import context 
from keras.utils import plot_model
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input, decode_predictions


def find_places(path):
    for root, dirs, files in os.walk(path, topdown=False):
        Vcontext = list()
        cont=0
        print("Begin directory")
        for name in files:
            if cont == 0 :
                       #Vcontext.append(context("Id"," Label",0))
                       cont = cont+1    
            img_path = os.path.join(root, name) 
            print(img_path)
            img = image.load_img(img_path, target_size=(224, 224)) 
            x =image.img_to_array(img) 
            x = np.expand_dims(x, axis=0) 
            #x = preprocess_input(x)
            #print (x.ndim, x.shape, 'x')
            model_objects = VGG16(weights='imagenet')
            print("Model Objects: ",model_objects)
            model_places = VGG16_Places365(weights='places')

            model_objects2=ResNet50(weights='imagenet')



            predictions_to_return = 1
            preds_places = model_places.predict(x)[0]
            preds_objects = model_objects.predict(x)
            preds_objects2 = model_objects2.predict(x)

            #lista_objetos = decode_predictions(preds_objects, top=5)[0]
            lista_objetos = decode_predictions(preds_objects, top=1)[0]
            lista_objetos2 = decode_predictions(preds_objects, top=3)[0]

            #lista_sitios = decode_predictions(preds_places, top=5)[0]
            probs_places = np.sort(preds_places)[::-1][0:len(preds_places)]
            #print ('preds', preds.ndim, preds.shape, np.sort(preds)[::-1][0:5])
            top_preds_places = np.argsort(preds_places)[::-1][0:len(preds_places)]
            plot_model(model_objects, to_file='model_plot_objects.png', show_shapes=True, show_layer_names=True)
            plot_model(model_places, to_file='model_plot_places.png', show_shapes=True, show_layer_names=True)
            print("Objetos: ")
            #print (lista_objetos[0][0],lista_objetos[0][1],lista_objetos[0][2])
            print ("Sitios: ")
            #print (preds_places)
           # print ('top_preds', top_preds.ndim, top_preds.shape, top_preds) 
            	# load class label 
            file_name ='categories_places365.txt'
            if not os.access(file_name, os.W_OK):
                synset_url = 'Caution-https://raw.githubusercontent.com/CSAILVision/places365/master/categories_places365.txt'
                os.system('wget ' + synset_url)            
            classes = list()
            with open(file_name) as class_file:
                for line in class_file:
                    classes.append(line.strip().split(' ')[0][3:]) 
                classes = tuple(classes) 
            	#print classes 
                print('--SCENE CATEGORIES Places: (id, description, prob)') 
            	# output the prediction
                for i in range(0, 3):
                   #print(top_preds_places[i], classes[top_preds_places[i]],probs_places[i])
                   Vcontext.append(context(top_preds_places[i],classes[top_preds_places[i]],probs_places[i]))
                Vcontext.sort(key=lambda context: context.label)
                for i in range(0, len(lista_objetos)):
                   Vcontext.append(context(lista_objetos[i][0],lista_objetos[i][1],lista_objetos[i][2]))
                
                print('Prediict:', decode_predictions(preds_objects2,top=3)[0])
                print ('File completed: ', len(Vcontext))

                #for i in range(0, len(lista_objetos2)):
                 #   Vcontext.append(context(lista_objetos2[i][0], lista_objetos2[i][1], lista_objetos2[i][2]))

                #print('File completed: ', len(Vcontext))

        for j in Vcontext:
            print (j)                
    Vcontext.clear()
# Editado por: Joana Estefanía Nicolalde Perugachi
# Correo: jenicolaldep@uce.edu.ec
# Fecha: 02/12/2020
# Places365 con red vgg16- Entrenamiento

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 09:21:28 2019

@author: felipe
"""


# Load the pre-trained model
# First, we will load a VGG model without the top layer ( which consists of fully connected layers ).

from keras.applications.vgg16 import VGG16
from keras.utils import plot_model
#Load the VGG model
#vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=(image_size, image_size, 3))
vgg_conv = VGG16(include_top=True, weights='imagenet')
vgg_conv.summary()
plot_model(vgg_conv, to_file='./graph_models/vgg_conv_model_plot.png', show_shapes=True, show_layer_names=True)
# Freeze the required layers
# In Keras, each layer has a parameter called “trainable”. 
# For freezing the weights of a particular layer, we should set this parameter to False, 
# indicating that this layer should not be trained. That’s it! 
# We go over each layer and select which layers we want to train.

# Freeze the layers except the last 4 layers
for layer in vgg_conv.layers[:-4]:
    layer.trainable = False
 
# Check the trainable status of the individual layers
for layer in vgg_conv.layers:
    print(layer, layer.trainable)
    
# Create a new model
# Now that we have set the trainable parameters of our base network, 
# we would like to add a classifier on top of the convolutional base. 
# We will simply add a fully connected layer followed by a softmax layer with 3 outputs. 
# This is done as given below.

from keras import models
from keras import layers
from keras import optimizers
 
# Create the model
model = models.Sequential()
 
# Add the vgg convolutional base model
model.add(vgg_conv)
 
# Add new layers
model.add(layers.Flatten())
model.add(layers.Dense(1024, activation='relu'))
model.add(layers.Dropout(0.5))
model.add(layers.Dense(3, activation='softmax'))
 
# Show a summary of the model. Check the number of trainable parameters
model.summary()



